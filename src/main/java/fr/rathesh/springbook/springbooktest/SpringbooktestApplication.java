package fr.rathesh.springbook.springbooktest;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;

@SpringBootApplication
public class SpringbooktestApplication {




	public static void main(String[] args) {
		SpringApplication.run(SpringbooktestApplication.class, args);
	}
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			try{
				Connection con= DriverManager.getConnection(
						"jdbc:postgres://localhost:5432/postgres","postgres","toto");

				Statement stmt=con.createStatement();
				ResultSet rs=stmt.executeQuery("select * from emp");
				while(rs.next())
					System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
				con.close();
			}catch(Exception e){ System.out.println(e);}
		};
	}
}
