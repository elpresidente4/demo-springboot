package fr.rathesh.springbook.springbooktest;


import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class ServiceBidonImpl implements ServiceBidon {
    @Override
    public void bidon() {

    }
}
